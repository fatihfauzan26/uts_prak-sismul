# No 1
![](https://gitlab.com/fatihfauzan26/uts_prak-sismul/-/raw/main/video1231272945.gif)
##### Link YouTube: https://youtu.be/FwiEat7b6lE
# No 2
```mermaid
flowchart TD
    A[start] --> B(input image file path)
    B --> C(store image to variable)
    C --> D(define kernel size) 
    D --> E(apply gaussian blur)
    E --> F(display processed image)
    F --> G(save image)
    G --> H(end)
  
```
# No 3
Program tersebut menggunakan beberapa aspek kecerdasan buatan (AI) dalam memproses citra digital, yaitu:

    1. Pengolahan Citra (Image Processing): Program ini menggunakan pustaka OpenCV (Open Source Computer Vision) yang merupakan salah satu pustaka terkenal untuk pengolahan citra digital. Pustaka ini menyediakan banyak fungsi dan metode untuk memanipulasi citra digital, seperti membaca, menulis, memperkecil, memperbesar, mengubah kontras, mengubah warna, memotong, dan banyak lagi.

    2. Filtering (Penghalusan): Program ini menggunakan teknik penghalusan citra dengan filter Gaussian. Gaussian blur digunakan untuk mengurangi noise pada citra dan menghasilkan citra yang lebih halus dan lebih mudah diolah. Teknik penghalusan citra ini memanfaatkan konsep distribusi normal untuk menentukan bobot dari setiap piksel di sekitarnya. Pada program ini, parameter kernel_size (7, 7) digunakan untuk menentukan ukuran kernel yang akan digunakan pada proses Gaussian blur.

    3. Pemrosesan Data Numerik: Program ini menggunakan pustaka NumPy untuk memanipulasi data numerik. NumPy adalah pustaka Python yang populer untuk pemrosesan data numerik dan ilmiah, termasuk dalam pengolahan citra. Pada program ini, NumPy digunakan untuk menghitung dimensi citra dan melakukan operasi resize (mengubah ukuran citra).

    4. Interaksi dengan Pengguna: Program ini memanfaatkan input dari pengguna untuk memproses citra yang diinginkan. Fungsi input() digunakan untuk meminta nama berkas gambar dari pengguna. Selain itu, program ini juga menampilkan hasil pengolahan citra kepada pengguna melalui antarmuka grafis menggunakan pustaka OpenCV.

Secara keseluruhan, program tersebut memanfaatkan beberapa teknik AI dan pustaka terkait untuk melakukan pengolahan citra digital dengan lebih efisien dan efektif.
# No 4
![](https://gitlab.com/fatihfauzan26/uts_prak-sismul/-/raw/main/gifaudio.gif)
##### Link YouTube: https://youtu.be/KVwDp9VafBU
# No 5
```mermaid
flowchart TD
    A[start] --> B(input audio file path)
    B --> C(input audio output name)
    C --> D(input pitch shift amount) 
    D --> E(apply high pitch)
    E --> F(save audio)
    F --> g(end)
```
# No 6
Program tersebut menggunakan beberapa aspek kecerdasan buatan (AI) dalam memproses suara, yaitu:

    1. Digital Signal Processing (DSP): Program ini menggunakan pustaka LibROSA dan Soundfile untuk memproses sinyal digital suara. LibROSA menyediakan banyak fungsi untuk analisis sinyal audio, termasuk memuat berkas audio, melakukan pitch shifting, menghitung spektrogram, dan banyak lagi. Soundfile digunakan untuk menulis hasil pitch shift ke dalam berkas audio yang baru.

    2. Pitch Shifting: Program ini melakukan pengubahan frekuensi sinyal audio, yang disebut dengan pitch shifting. Pitch shifting berguna untuk mengubah nada suara, sehingga suara yang asli dapat disesuaikan dengan kebutuhan pengguna. Pada program ini, fungsi librosa.effects.pitch_shift() digunakan untuk melakukan pitch shifting pada sinyal audio dengan memasukkan parameter jumlah langkah nada yang ingin diubah (n_steps) dan nilai sampling rate yang baru (sr_lebih_cepat).

    3. Interaksi dengan Pengguna: Program ini memanfaatkan input dari pengguna untuk memproses file audio yang diinginkan. Fungsi input() digunakan untuk meminta nama berkas audio dari pengguna, serta nilai pitch shift yang diinginkan. Program juga menampilkan pesan ketika proses pitch shifting selesai dilakukan.

Secara keseluruhan, program tersebut memanfaatkan beberapa teknik AI dan pustaka terkait untuk melakukan pitch shifting pada file audio dengan lebih mudah dan efektif. Dalam pengolahan suara, AI juga dapat digunakan untuk melakukan pengenalan suara, sintesis suara, dan lain sebagainya
